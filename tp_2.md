# TP2 : Manipulation De Service

## Intro

On commence par nommer la machine.

```markdown
-Sudo hostname etienne.tp2.linux
on rennome la machine
-sudo nano etc/hostname
on va donner le nouveau nom dans le dossier hostname
-cat etc/hostname
on verifie que la modification a bien marcher et cela retourne bien etienne.tp2.linux
```
On vérifie le réseau

```markdown
-ping 1.1.1.1
-ping ynov.com
-ip a
-ping 192.168.202.
le résultat montre que la machine est connecté
```

## Partie 1 : SSH

On installe et on lance le serveur

```markdown
-sudo apt install openssh-server
on installe ssh sur la machine
-systemctl start sshd
on lance le serveur
-systemctl status sshd
on verifie que le lancement a marcher (on peux lire Enable donc le serveur marche) 
-sudo systemctl enable ssh
on utilise cette commande pour que le serveur ce lance a chaque lancement de la machine
```

Etude du service SSH

```markdown
on regarde les processus lié au service ssh
-ps -A | grep -F ssh
537 ? 00:00:00 sshd
919 ? 00:00:00 ssh-agent
1898 ? 00:00:00 sshd
1979 ? 00:00:00 sshd
on regarde les port utilisé par le service ssh
-ss -l | grep -F ssh
u_str   LISTEN   0        4096             /run/user/1000/gnupg/S.gpg-agent.ssh 25774                                           * 0
u_str   LISTEN   0        10                         /run/user/1000/keyring/ssh 26333                                           * 0
u_str   LISTEN   0        128                   /tmp/ssh-V2mAY3Fd8LXH/agent.836 26061                                           * 0
u_str   LISTEN   0        64               /var/lib/fwupd/gnupg/S.gpg-agent.ssh 47418                                           * 0
tcp     LISTEN   0        128                                           0.0.0.0:ssh                                       
0.0.0.0:*
tcp     LISTEN   0        128                                              [::]:ssh                                          
[::]:*
on se déplace dans le fichier log
-cd /var/log
on regarde les log du service ssh
-journalctl | grep -F ssh
oct. 25 15:52:39 toto.tp2.linux polkitd(authority=local)[459]: Operator of unix-session:c2 successfully authenticated as unix-user:etienne to gain TEMPORARY authorization for action org.freedesktop.systemd1.manage-unit-files for system-bus-name::1.85 [systemctl enable sshd] (owned by unix-user:etienne)
oct. 25 15:55:44 toto.tp2.linux sudo[2097]:  etienne : TTY=pts/0 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl enable sshd
oct. 25 15:56:06 toto.tp2.linux sudo[2102]:  etienne : TTY=pts/0 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl disable sshd
oct. 25 15:56:09 toto.tp2.linux sudo[2123]:  etienne : TTY=pts/0 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl enable sshd
oct. 25 15:56:34 toto.tp2.linux sudo[2127]:  etienne : TTY=pts/0 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl enable ssh
oct. 25 15:57:00 toto.tp2.linux sudo[2792]:  etienne : TTY=pts/0 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl enable ssh
oct. 25 16:36:13 node1.tp2.linux sshd[1979]: Received disconnect from 192.168.202.1 port 49687:11: disconnected by user
oct. 25 16:36:13 node1.tp2.linux sshd[1979]: Disconnected from user etienne 192.168.202.1 port 49687
oct. 25 16:36:13 node1.tp2.linux sshd[1898]: pam_unix(sshd:session): session closed for user etienne
oct. 25 16:36:57 node1.tp2.linux sshd[25103]: Accepted password for etienne from 192.168.202.1 port 61834 ssh2
oct. 25 16:36:57 node1.tp2.linux sshd[25103]: pam_unix(sshd:session): session opened for user etienne by (uid=0)
```

on modifie la configuration du serveur

```markdown
on modifie le port d'écoute du serveur ssh
-sudo nano /etc/ssh/sshd_config
on affiche la modification 
-cat /etc/ssh/sshd_config
Include /etc/ssh/sshd_config.d/*.conf
Port 2222
#AddressFamily any
#ListenAddress 0.0.0.0
#ListenAddress ::
```

## Partie 2 : FTP

Installation et lancement du service FTP

```markdown
on install le service FTP
-sudo apt get install vsftpd
on lance le servvice FTP
-sudo systemctl start vsftpd
On vérifie qu'il est bien lancé
-systemctl status
state running
on fait en sorte que le service FTP ce lance a chaque lancement de la machine
-sudo systemctl enable vsftpd
```

Etude du service FTP

```markdown
on regarde les processus lié au service FTP
-ps -A | grep -F vsftpd
698 ?        00:00:00 vsftpd
on regarde les port du systeme
ss -l | grep -F ftp
tcp   LISTEN 0      32                                              *:ftp                           *:*
on regarde les logs du systeme FTP
journalctl | grep -F ftp
nov. 07 17:09:34 serveur sudo[532]:  etienne : TTY=tty1 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
nov. 07 17:11:09 serveur sudo[720]:  etienne : TTY=tty1 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/systemctl start vsftpd
nov. 07 17:15:00 serveur sudo[739]:  etienne : TTY=tty1 ; PWD=/home/etienne ; USER=root ; COMMAND=/usr/bin/apt install vsftpd
```

Modification de la configuration du serveur

```markdown
on va dans le fichier de configuration
sudo nano /etc/vsftpd.conf
on enlève le # devant chown_upload=YES
on change le port en changeant le nombre sur la ligne
connect_from_port_2104=YES
```

## Partie 3 : Création de votre propre service

Instalation et jeu avec netcat

```markdown 
on instale netcat
-sudo apt get install netcat
on utilise la première machine comme serveur découte
-nc -l 1054
- on connecte la deuxième machine a notre serveur
-nc 192,168,56,102 1054
on écrit un message
-coucou
le message s'affiche sur l'autre machine
coucou
on utilise la machine comme serveur mais cette fois-ci on va la faire entrer les données dans un fichier
-nc -l 192,168,56,102 1054 > Chat
on se connecte avec l'autre machine
-nc   192,168,56,102 1054
on écrit un message
-bonjour
on utilise une commande pour voir le contenu du fichier ou on a envoyé le message
- cat Chat
bonjour
```

Un service basé sur netcat

```markdown
on crée un fichier service qui serra notre chat
-sudo nano /etc/systemd/system/chat_tp2.service 
on donne tout les accès a notre service chat
-sudo chmod 777chat_tp2.service
on regarde ou ce situe la commande nc dans notre machine
-which nc
usr/bin/nc
on utilise une machine comme serveur pour écrire directement dans le fichier servirce
-nc -l 192,168,56,102 1054 > etc/systemd/system/chat_tp2.service
on se connecte a notre service
-nc 192,168,56,102 1054
on écrit dans le fichier service
-[Unit]
Description=Little chat service (TP2)
[Service]
ExecStart=/usr/bin/nc -l 192,168,56,102 1054
[Install]
WantedBy=multi-user.target
on relance le service
-sudo systemctl daemon-reload
```

Les Test

```markdown
on lance notre systeme 
-sudo systemctl start chat_tp2
on vérifie que le lancement a bien marché
-sudo systemctl status
on regarde que le systeme est sur le bon port d'écoute
-ss -l
tcp listen 0 1 0,0,0,0:1054
on regarde les log pour vérifié que le systeme marche
-journalctl -xe -u chat_tp2 -f 
je t’aime
lol
```
