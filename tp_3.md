# TP 3 : A little Script

## I : Script 

script utiliser sur linux ubuntu

[cliquez-ici](https://gitlab.com/ETIENNECHALAYE/tp-linux/-/blob/main/idcard.sh) pour acceder au script.

```markdown
script idcard.sh
commande de lancement : sudo bash idcar.sh
output:
Machine name : node1.tp2.linux

Os Ubuntu and kernel version is 5.11.0-38-generic

IP : 10.0.2.15 192.168.202.13

Ram : 1.14Go RAM restante sur 1.93Go RAM totale

Disque : 3G space left

Top 5 processes by RAM usage :
/usr/lib/firefox/firefox
/usr/lib/firefox/firefox
/usr/lib/firefox/firefox
xfwm4
-core

Listening ports:
402 : systemd-r
444 : cupsd
444 : cupsd
542 : sshd
542 : sshd

idcard.sh: line 8: curl: command not found
Here's your random cat :
l'image de chat ne s'affiche pas.
```

## II : Script youtube-dl

script utiliser sur linux manjaro

[cliquez-ici](https://gitlab.com/ETIENNECHALAYE/tp-linux/-/blob/main/yt.sh) pour acceder au script.

[cliquez-ici](https://gitlab.com/ETIENNECHALAYE/tp-linux/-/blob/main/download.log) pour acceder au log.

```markdown
script yt.sh
commande de lancement : sudo sh yt.sh "https://www.youtube.com/watch?v=9LzQimPV1t8"
ce qui affiche :
/srv/yt/downloads/Vidéo Courte et Drôle :) | #27
video https://www.youtube.com/watch?v=9LzQimPV1t8 was downloaded
File path : /srv/yt/downloads/Vidéo Courte et Drôle :) | #27 / Vidéo Courte et Drôle :) | #27 .mp4

pour les log on obtient : cat download.log
ce qui donne :
21/11/22 23:23:12 Video https://www.youtube.com/watch?v=9LzQimPV1t8 was downloaded. File path : /srv/yt/downloads/Vidéo Courte et Drôle :) | #27
```

## III : Make it a service

Je n'ai malheureusement pas réussi a fini la partie 3.

![CHAT](/picture/chat_sorry.jpeg)
