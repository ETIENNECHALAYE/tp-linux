# TP5 : Petit Cloud Perso

## I : Setup DB

**Installer MariaDB**

```markdown
[etienne@db ~]$sudo dnf install mariadb-server

[etienne@db ~]$sudo systemctl start mariadb
[etienne@db ~]$sudo systemctl enable mariadb
 # output : Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
 # Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
 # Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/m
[etienne@db ~]$systemctl status mariadb
 # output :    Active: active (running) since Thu 2021-11-25 17:20:43 CET; 4min 22s ago
[etienne@db ~]$sudo ss -lntp 
 # output : LISTEN                   0                        80                                                       *:3306                                                  *:*                       users:(("mysqld",pid=26673,fd=21))
[etienne@db ~]$sudo ps -ef | grep mysql
 # output : mysql      26673       1  0 17:20 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
 # etienne    26796    1590  0 17:33 pts/0    00:00:00 grep --color=auto mysql
[etienne@db ~]$sudo firewall-cmd --add-port=80/tcp --permanent
 # output : success
```

**Configurer MariaDB**

configuration elementaire de la base

```markdown
[etienne@db ~]$ sudo mysql_secure_installation

 # question1 : Set root password? [Y/n] Y
 # on dit oui pour plus de sécurité

 # question2 : Remove anonymous users? [Y/n] Y
 # on dit oui vu qu'on a pas besoin d'utilisateur anonyme

 # question3 : Disallow root login remotely? [Y/n] Y
 # on empeche root de pouvoir ce connecté a distance

 # question4 : Remove test database and access to it? [Y/n] Y
 # on supprime les choses inutile au quel les gens peuvent en plus acceder (principe du moindre privilège ;) )

 # question5 : Reload privilege tables now? [Y/n] Y
 # les modification sur les base de données prendrons effet instantanement donc plus de rapidité de travail
```

preparation de la base en vu de l'utilisation de nextcloud

```markdown
[etienne@db ~]$ sudo mysql -u root -p
MariaDB [(none)]> CREATE USER 'nextcloud'@'10.5.1.11' IDENTIFIED BY 'meow';
# output : Query OK, 0 rows affected (0.000 sec)
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
# output : Query OK, 1 row affected (0.000 sec)
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
# output : Query OK, 0 rows affected (0.000 sec)
MariaDB [(none)]> FLUSH PRIVILEGES;
# output : Query OK, 0 rows affected (0.000 sec)
```

**Test**

```markdown
[etienne@web ~]$ sudo dnf provides mysql
# mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
[etienne@web ~]$ sudo dnf install mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64

[etienne@web ~]$ mysql --port=3306 --host=10.5.1.12 --user=nextcloud --password nextcloud
mysql> SHOW TABLES;
# output : Empty set (0.00 sec)
```

## II : Setup Web

**Install Apache**

Apache

```markdown
[etienne@web ~]$ sudo dnf install httpd

[etienne@web ~]$ sudo systemctl start httpd
[etienne@web ~]$ ps -ef | grep httpd
# output : root        1990       1  0 18:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1991    1990  0 18:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1992    1990  0 18:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1993    1990  0 18:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      1994    1990  0 18:00 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
etienne     2207    1470  0 18:00 pts/0    00:00:00 grep --color=auto httpd
[etienne@web ~]$ sudo ss -lntp | grep httpd
# output : LISTEN 0      128                *:80              *:*    users:(("httpd",pid=1994,fd=4),("httpd",pid=1993,fd=4),("http
",pid=1992,fd=4),("httpd",pid=1990,fd=4))

[etienne@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
# output : success
[etienne@web ~]$ sudo firewall-cmd --reload
# output : success
[etienne@web]$ curl http://10.5.1.11:80
# output : <!doctype html>
<html>
  <head>
    <meta charset='utf-8'>
```

PHP

install PHP

```markdown
[etienne@web ~]$ sudo dnf install epel-release
[etienne@web ~]$ sudo dnf update
[etienne@web ~]$ sudo dnf install https://rpms.remirepo.net/enterprise/remi-release-8.rpm
[etienne@web ~]$ sudo dnf module enable php:remi-7.4
[etienne@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

**Conf Apache**

```markdown
[etienne@web ~]$ cat /etc/httpd/conf/httpd.conf
# output : Include conf.modules.d/*.conf

[etienne@web conf.d]$ sudo nano Vhost.conf
[etienne@web conf.d]$ cat Vhost.conf
# output : <VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[etienne@web nextcloud]$ sudo systemctl restart httpd

[etienne@web www]$ sudo mkdir nextcloud
[etienne@web nextcloud]$ sudo mkdir html
[etienne@web www]$ sudo chown apache nextcloud
[etienne@web www]$ ls -l
# output : drwxr-xr-x. 3 apache root 18 Nov 28 11:40 nextcloud
[etienne@web nextcloud]$ sudo chown apache html
[etienne@web nextcloud]$ ls -l
# output : drwxr-xr-x. 2 apache root 6 Nov 28 11:40 html

[etienne@web nextcloud]$ sudo nano /etc/opt/remi/php74/php.ini
[etienne@web nextcloud]$ cat  /etc/opt/remi/php74/php.ini
#output : date.timezone = "Europe/Paris"
```

**Install Nextcloud**

```markdown
[etienne@web nextcloud]$ cd
[etienne@web ~]$ sudo curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[etienne@web ~]$ ls
# output : nextcloud-21.0.1.zip

[etienne@web ~]$ sudo unzip nextcloud-21.0.1.zip
[etienne@web ~]$ sudo mv nextcloud /var/www/nextcloud/html/
[etienne@web ~]$ sudo rm nextcloud-21.0.1.zip
```

**Test**

