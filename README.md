# TP Linux

## Objectif Global

L'objectif est de détruire 5 machine virtuel de manière original. Une machine virtuel est dite détruite a partir du moment où elle est inutilisable.

## Rendu de tp

**première machine virtuelle:**
```markdown
- commande : sudo rm -rf /root
- résultat : la machine virtuel se lance mais elle affiche un écran noir.
```

**deuxième machine virtuelle:**
```markdown
- commande : which bash
- objectif : trouver le fichier de la commande bash
- commande : sudo chmod -x /usr/bin/bash
- résultat : Bash ne se lançant plus on peux toujours acceder a l'espace de connexion 
mais lorsqu'on se connecte a notre session on retourne sur l'espace de connexion.
```

**troisième machine virtuelle:**
```markdown
- on commence par télécharger une image 
- commande : sudo apt install fim
- objectif : pouvoir afficher l'image.
- création d'un fichier txt dans lequel on entre la commande : while (( ; ; )) do fim /home/etienne/Downloads/truc_a_ouvrir.txt done
- commande : chmod +x /home/etienne/Downloads/text.txt
- ojectif : transformer le fichier txt en un éxécutable.
- commande : nano /home/etienne/Documents/lancement.sh
- dans lancement.sh on écrit « # !/bin/sh puis bash test.txt
- maintenant on va faire éxécuté le programme quand on lance un terminal
- commande : sudo -i
- commande : chmod -R 777 /home/gaby/Documents/lancement.sh
- commande : nano /home/gaby/.bashrc
- on écrit a la fin du fichier bashrc : /home/gaby/Documents/lancement.sh
- résultat : lors du lancement d'un terminal une image apparait et quand on la ferme elle se réouvre de ce fait le terminal est inutilisable.
```

**quatrième machine virtuelle:**
```markdown
- on télécharge une nouvelle image 
- on installe a nouveau fim
- on crée un fichier txt on y écrit while (( ; ; )) do fim /home/etienne/Downloads/truc_a_ouvrir.txt reboot done
- commande : chmod +x /home/etienne/Documents/lancement.sh
- de ce fait on va afficher une image et lors de sa fermeture l'ordinateur redemarera.
- on crée a nouveau un fichier lancement.sh
- on refait exactement comme avec la machine virtuelle précédente pour pouvoir lancer notre fichier quand on lance le terminal.
- résultat : lors de l'ouverture d'un terminal, une image apparait et lors de sa fermeture l'ordinateur redémarre.
```

**cinquième machine virtuelle:**
```markdown
- commande : rm -rf /usr/bin/bash
- résultat : on ne peux que ce connecter mais dès que l'on ce connecte a notre session on retourne sur la page de connexion.
- A la base pour cette machine virtuelle je voulais crée un fichier txt
qui s'exécutais a l'ouverture du terminal et supprimais tout les fichier
que j'écrivais soit le fichier Documents, le fichier etienne, 
le fichier home et enfin le fichier root. Seul problème cela marchait uniquement
pour le fichier Document et etienne mais le reste me mettais une permission deneid. 
Donc je n'ai pas put faire ce que je voulais.
```
 
