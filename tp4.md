# TP4 : Une distribution orientée serveur

## Checklist

**Ip static**

```markdown
fichier conf :
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.56
NETMASK=255.255.255.0
resultat a un ip a :
enp0s8 : 10.250.1.56/24
```

**connexion ssh fonctionnelle**

```markdown
commande : sudo systemctl status sshd
output : active
commande pour afficher la clé publique : cat id_rsa.pub
clé : ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+/mgIJK5UqQAF6FXMzZv9Rd3n+gUc348W1rmcoN+DBVfoALeWwNaf7em24e+pGxuXaC8aLaZIw6nu6+O2IWpLZ3RNB9FsUugOU0Abs9p8VrX8mHdH4TgeXRaBBNdyoc9nkvjZA4edlh9Y44iFu91eMbrUmMvlJuzAN3bbkAcpaVMaZfjPy2y7Q/QHp/PNO/cvVh7xSGDh/CNlUpBqPGJzwZAo2f/cxwOiMMyOUZRh0C6Fx5g6hmIxg2e2Es9Owc4xGO/ZXs8NhszgtEt2dY6iEMsXMkGURHlFyaG/vknTUuEOOx0vpy80nPqDUD/HFJ7l1vxHlJg3Zywn8P8J+BtES/64ps9w5e4fnUXL2uH4aCDBOkNnFSfh+0BomZ+P2cr0Z0vZymV/7xW0fdRFmSjDE44sfdTbz38li2mRUV9adHCRdqzkF2yp+h2QuBoYMUl2Q6PvMR0P9w/NyuHJvCSZjK7hh1wY3Oc+Oa7RMXWD4Oy+SEdkX2YZGptKzlSAS8jbTHUCe6tk5KimU3zl5eaeBc9k1VlIR6OUS+Eizs2+1KO1gCDeHeh/QUT95D88vSivaGwSHATaEE0lTDPUGqHyaOhORtLIavzbt4IyLKLtXqB9mUMkRyzGAALoLD/Pq7Ri5Ort6vXf3LbzRPbDABj1rBhoVodIfTkVGX5+GpaBFw== etien@DESKTOP-CCLQQ92
commande pour affiché les clé autorisé : cat authorized_keys
clé autorisé : ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC+/mgIJK5UqQAF6FXMzZv9Rd3n+gUc348W1rmcoN+DBVfoALeWwNaf7em24e+pGxuXaC8aLaZIw6nu6+O2IWpLZ3RNB9FsUugOU0Abs9p8VrX8mHdH4TgeXRaBBNdyoc9nkvjZA4edlh9Y44iFu91eMbrUmMvlJuzAN3bbkAcpaVMaZfjPy2y7Q/QHp/PNO/cvVh7xSGDh/CNlUpBqPGJzwZAo2f/cxwOiMMyOUZRh0C6Fx5g6hmIxg2e2Es9Owc4xGO/ZXs8NhszgtEt2dY6iEMsXMkGURHlFyaG/vknTUuEOOx0vpy80nPqDUD/HFJ7l1vxHlJg3Zywn8P8J+BtES/64ps9w5e4fnUXL2uH4aCDBOkNnFSfh+0BomZ+P2cr0Z0vZymV/7xW0fdRFmSjDE44sfdTbz38li2mRUV9adHCRdqzkF2yp+h2QuBoYMUl2Q6PvMR0P9w/NyuHJvCSZjK7hh1wY3Oc+Oa7RMXWD4Oy+SEdkX2YZGptKzlSAS8jbTHUCe6tk5KimU3zl5eaeBc9k1VlIR6OUS+Eizs2+1KO1gCDeHeh/QUT95D88vSivaGwSHATaEE0lTDPUGqHyaOhORtLIavzbt4IyLKLtXqB9mUMkRyzGAALoLD/Pq7Ri5Ort6vXf3LbzRPbDABj1rBhoVodIfTkVGX5+GpaBFw== etien@DESKTOP-CCLQQ92
connexion sans mdp : ssh etienne@10.250.1.56
output : Activate the web console with: systemctl enable --now cockpit.socket
Last login: Tue Nov 23 17:26:40 2021 from 10.250.1.1
```

**connexion internet**

```markdown
commande : ping 8.8.8.8
output : 64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=22.10 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=26.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=25.5 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=26.0 ms
commande : ping ynov.com
output : 64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=51 time=22.5 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=51 time=22.9 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=51 time=22.8 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=51 time=21.5 ms
```

**nommage de la machine**

```markdown
commande : sudo hostname node1.tp4.linux
donc on change le nom de la machine puis on applique le changement pour chaque rédémarage.
commande : sudo nano /etc/hostname 
on change le nom de la machine en écrit node1.tp4.linux
commande cat /etc/hostname
output : node1.tp4.linux
```

## Mettre en place un service

**2 instal du service**

```markdown
commande : sudo dnf nginx
l'installation est fini.
```

**3 Analyse**

```markdown
commande : sudo systemctl start nginx
on lance le systeme 
commande : systemctl status nginx
output :  Active: active (running) since Wed 2021-11-24 16:48:04 CET; 1min 23s ago

commande : ps -ef | grep -F nginx
output : root        1517       1  0 16:48 ?        00:00:00 nginx: master process /usr/sbin/nginx
nginx       1518    1517  0 16:48 ?        00:00:00 nginx: worker process
etienne     1572    1436  0 16:57 pts/0    00:00:00 grep --color=auto -F nginx

commande : sudo ss -lntp | grep nginx
output : LISTEN 0      128          0.0.0.0:80        0.0.0.0:*    users:(("nginx",pid=1518,fd=8),("nginx",pid=1517,fd=8))
LISTEN 0      128             [::]:80           [::]:*    users:(("nginx",pid=1518,fd=9),("nginx",pid=1517,fd=9))

commande : cd /usr/share/nginx/html 
commande : ls 
output : 404.html  50x.html  index.html  nginx-logo.png  poweredby.png

commande : ls -l
output : -rw-r--r--. 1 root root 3332 Jun 10 11:09 404.html
-rw-r--r--. 1 root root 3404 Jun 10 11:09 50x.html
-rw-r--r--. 1 root root 3429 Jun 10 11:09 index.html
-rw-r--r--. 1 root root  368 Jun 10 11:09 nginx-logo.png
-rw-r--r--. 1 root root 1800 Jun 10 11:09 poweredby.png
```

**4 Visite du service web**

configurez le firewall

```markdown
commande :  sudo firewall-cmd --add-port=80/tcp --permanent
output : success

commande : sudo firewall-cmd --reload
output : success
```

testez le bon fonctionnement du service

```markdown
commande : curl http://10.250.1.56:80
output : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
```

**5 Modif de la conf du serveur web 

Changer le port d'écoute

```markdown
commande : sudo nano  /etc/nginx/nginx.conf
commande : cat /etc/nginx/nginx.conf
output :         listen       8080 default_server;
        listen       [::]:8080 default_server;

commande : sudo systemctl restart nginx
commande : systemctl status nginx
output :    Active: active (running) since Wed 2021-11-24 17:31:34 CET; 11s ago

commande : sudo ss -lnpt | grep nginx
output : LISTEN 0      128          0.0.0.0:8080      0.0.0.0:*    users:(("nginx",pid=1711,fd=8),("nginx",pid=1710,fd=8))
LISTEN 0      128             [::]:8080         [::]:*    users:(("nginx",pid=1711,fd=9),("nginx",pid=1710,fd=9))

commande : sudo firewall-cmd --remove-port=80/tcp --permanent
output : success
commande : sudo firewall-cmd --add-port=8080/tcp --permanent
output : success
commande : sudo firewall-cmd --reload
output : success
commande : curl http://10.250.1.56:8080
output : <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
```

Changer l'utilisateur qui lance le service

```markdown
commande : sudo useradd -d /home/web -p pipi  web
commande : cd /home
commande : ls
output : etienne web
commande : cat /etc/passwd | grep web
output : web:x:1001:1001::/home/web:/bin/bash

commande : sudo nano /etc/nginx/nginx.conf
commande : cat /etc/nginx/nginx.conf
output : user web;

commande : ps -ef | grep nginx
output : web         1816    1815  0 17:55 ?        00:00:00 nginx: worker process
```

Changer l'emplacement de la racine web

```markdown
commande : cd /var
commande : mkdir www
commande : cd www
commande : mkdir super_site_web
commande : sudo nano index.html

commande : sudo chown web /var/www
commande : sudo chown web /var/www/super_site_web
commande : sudo chown web /var/super_site_web/index.html
commande : ls - l
output : -rw-r--r--. 1 web root 33 Nov 24 18:03 index.html
cela output la même chose pour chaque dossier ou on a fait la commande chown web

commande : sudo nano /etc/nginx/nginx.conf
commande : cat /etc/nginx/nginx.conf
output :   root         /var/wwww/super_site_web;

commande : sudo systemctl restart nginx 
commande : curl http://10.250.1.56:8080
output : <h1>Le TP4 était super dure</h1>
```
